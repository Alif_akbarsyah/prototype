<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />  
  <title>Page Title</title>
  <style>
    .nav-item{
      margin-right: 4px;
    }
    .nav-link{
      color: black;
    }
    .atur{
      margin-right: 30px;    
    }
    nav{
      box-shadow: 10px 14px 50px 2px #laaala;       
    }
    .navbar-expand-lg{
      box-shadow: 3px -3px 10px 1px black; 
    }
    /* .ntap{
      width: 130px;
  box-sizing: border-box;
  border: 2px solid #ccc;
  border-radius: 4px;
  font-size: 16px;
  background-color: white;
  background-image: url('searchicon.png');
  background-position: 10px 10px; 
  background-repeat: no-repeat;
  padding: 12px 20px 12px 40px;
  -webkit-transition: width 0.4s ease-in-out;
  transition: width 0.4s ease-in-out;
    }
    .ntap:focus{
      width:100%;
    } */
  </style>
  
</head>
<body>
<nav class="navbar sticky-top navbar-expand-lg navbar-light bg-white ">
  <a class="navbar-brand" href="#"><img src="foto_produk/merci.jpg" width="70" height="70" class="d-inline-block align-top" alt=""></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    
    <ul class="navbar-nav ml-auto">
      <li class="nav-item atur">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item atur">
        <a class="nav-link" href="shop.php">Shop</a>
      </li>           
      <li class="nav-item atur">
        <a class="nav-link" href="about.php">About</a>
      </li>           
      <!-- <form>
        <input type="text" class="ntap"name="search" placeholder="Search..">
      </form> -->
      
      <li class="nav-item" >
        <a class="nav-link aga" href="keranjang.php"><i class="fas fa-cart-plus"></i><span class="caret"></span></a>
      </li>    
      <span class="ogo">|</span>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" data-target="dropdown_target" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user"></i>
          <span class="caret"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown_target">
        <?php if(isset($_SESSION["pelanggan"])): ?>                    
                    <a class="dropdown-item" href="profile.php">Profile</a>
                    <a class="dropdown-item" href="riwayat.php">Riwayat</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="logout.php">Logout</a>
                <?php else: ?>                    
                    <a class="dropdown-item" href="daftar.php">Login</a>
                    <a class="dropdown-item" href="Daftar.php">Daftar</a>                  
        <?php endif ?>            
        </div>
      </li> 
    </ul>    
  </div>
</nav>
</body>
</html>
