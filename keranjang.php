<?php
    session_start();
    include 'bootstrap.php';
    include 'koneksi.php';


if(empty($_SESSION["keranjang"]) or !isset($_SESSION["keranjang"]))
{
    echo "<script>alert('Mohon Belanja Terlebih dahulu'); location= 'shop.php';</script>";
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />    
    <link rel="icon" type="image/png" sizes="32x32" href="foto_produk/fav.png">
    <title>Keranjang</title>    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">    
    <link rel="stylesheet" href="css/cart.css" />    
    <script
        src="http://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous">
    </script>
</head>
<body>
<?php include 'navbar4.php'; ?>

<section class="konten">
    <div class="container">
        <br><br>
        <h1>Keranjang Belanja</h1>
        <hr>
        <table class="table table-borderless table-shopping" style="box-sizing: border-box;">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Produk</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Jumlah</th>
                    <th scope="col">SubHarga</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                
                <?php 
                    $no = 1;
                    foreach ($_SESSION["keranjang"] as $id_produk => $jumlah) :
                        // menampilkan produk 
                    $ambil = $koneksi->query("SELECT * FROM produk where id_produk = '$id_produk' ");
                    $pecah = $ambil->fetch_assoc();
                    $subharga = $pecah["harga_produk"]* $jumlah;                    
                ?>
                <tr class="coba">
                    <td class="colm-1"><img src="foto_produk/<?php echo $pecah["foto_produk"]; ?>" width="180px" alt=""></td>
                    <td><?php echo $pecah["nama_produk"]; ?></td>
                    <td>Rp. <?php echo number_format($pecah["harga_produk"]); ?></td>
                    <td><?php echo $jumlah; ?></td>                    
                    <td><?php echo number_format($subharga); ?></td>
                    <td style="padding-top:55;">
                        <a href="hapuskeranjang.php?id=<?php echo $id_produk ?>" class="btn btn-danger btn-xs">Hapus</a>
                    </td>
                    
                </tr>

                <?php
                    $no++;
                 endforeach 
                 ?>
            </tbody>
            <tfoot class="coba">
                <tr>
                    <th colspan="5"></th>
                    <th>
                        <a href="shop.php" class="btn btn-default">Lanjut Belanja</a>
                        <a href="checkout.php" class="btn btn-primary">Checkout</a>
                    </th>
                </tr>
            </tfoot>
        </table>
        
    </div>
</section>        
</body>
</html>