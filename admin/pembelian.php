<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<h2>Data Pembelian</h2>

<table class="table table-bordered" >

    <tr>
      <th>No.</th>
      <th>Nama Pelanggan</th>
      <th>Tanggal Pinjam</th>
      <th>Tanggal Kembali</th>
      <th>Status Pembayaran</th>
      <th>Total</th>      
      <th>Aksi</th>      
    </tr>
    <tr>
        <?php
        $no =1;
        $ambil= $koneksi->query("select * from pembelian join pelanggan on pembelian.id_pelanggan 
        = pelanggan.id_pelanggan");
            while($pecah = $ambil->fetch_assoc()){

        ?>
        <td><?php echo $no ?></td>
        <td><?php echo $pecah['nama_pelanggan']; ?></td>
        <td><?php echo $pecah['tanggal_pinjam']; ?> </td>
        <td><?php echo $pecah['tanggal_kembali']; ?> </td>
        <td><?php echo $pecah['status_pembelian']; ?> </td>
        <td><?php echo $pecah['total_harga']; ?> </td>
        <td>          
           
            <a href="index.php?halaman=detail&id=<?php echo $pecah['id_pembelian'] ?>" class="btn btn-info"><i class="fas fa-info-circle"></i></a>

            <?php if($pecah['status_pembelian']== "sudah kirim pembayaran"): ?>
            <a href="index.php?halaman=pembayaran&id=<?php echo $pecah['id_pembelian'] ?>" class="btn btn-success"><i class="fas fa-clipboard-check"></i></a>
            <?php endif?>
            
            <?php $angka= date('Y-m-d'); ?>
       

            <?php if($pecah['status_pembelian'] == "pending" || $pecah['status_pembelian'] == "sudah kirim pembayaran" ): ?>            
            <form method="post" style="float:left; margin-right:4px;">
            <input type="hidden" name="kankel" value="<?php echo $pecah['id_pembelian'] ?>">
                <input type="hidden" name="stat" value="Cancel">
                <button class="btn btn-danger" name="tangkap"><i class="fas fa-times"></i>
                </button>
            </form>
            <?php
            include '../koneksi.php';
                 if(isset($_POST["tangkap"]))
                 {                     
                    $id_pem =  $_POST["kankel"];
                     $status = $_POST["stat"];
                     $koneksi->query("update pembelian set status_pembelian= '$status' 
                     where id_pembelian = '$id_pem' ");
                     echo "<script>alert('Data Sudah Di Update'); location= 'index.php?halaman=pembelian';</script>";
                 }
            ?>
            <?php endif?>
            <?php if($pecah['status_pembelian'] == "Proses Pengembalian" ): ?>            
            <form method="post" style="float:left; margin-right:4px;">
            <input type="hidden" name="kankel" value="<?php echo $pecah['id_pembelian'] ?>">
                <input type="hidden" name="stat" value="Berhasil Dikembalikan">
                <button class="btn btn-success" name="ceklis"><i class="fas fa-check"></i>
                </button>
            </form>
            <?php
            include '../koneksi.php';
                 if(isset($_POST["ceklis"]))
                 {                     
                    $id_pem =  $_POST["kankel"];
                     $status = $_POST["stat"];
                     $koneksi->query("update pembelian set status_pembelian= '$status' 
                     where id_pembelian = '$id_pem' ");
                     echo "<script>alert('Data Sudah Di Update'); location= 'index.php?halaman=pembelian';</script>";
                 }
            ?>
            
            </button>
                <?php endif?>
        </td>
    </tr>
    <?php
        $no++;
            }
    ?>
</table>