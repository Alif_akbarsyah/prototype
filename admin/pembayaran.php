<h2>Data Pembayaran</h2>

<?php
include '../koneksi.php';
    $id_pem = $_GET['id'];

    $ambil= $koneksi->query("select * from pembayaran where id_pembelian = '$id_pem'");
    $detail = $ambil->fetch_assoc();
    
?>
<div class="row">
    <div class="col-md-6">
        <table class="table">
            <tr>
                <th>Nama</th>
                <td><?php echo $detail['nama_pembayaran']; ?></td>
            </tr>
            <tr>
                <th>Bank</th>
                <td><?php echo $detail['bank']; ?></td>
            </tr>
            <tr>
                <th>Jumlah</th>
                <td>Rp. <?php echo number_format($detail['jumlah_pembayaran']); ?></td>
            </tr>
        </table>
    </div>
    <div class="col-md-6">
        <img src="../bukti_pembayaran/<?php echo $detail['bukti'] ?>" class="img-responsive">
    </div>
</div>

<form method="post">
    <div class="form-group">
        <label>No. Resi</label>
        <input type="text" class="form-control" name="resi">
    </div>
    <div class="form-group">
        <label>Status</label>        
        <select name="status" class="form-control">
        <option value="">Pilih Status</option>
        <option value="lunas">Lunas</option>
        <option value="barang dikirim">Mobil Dikirim</option>
        <option value="batal">Batal</option>
        </select>
    </div>
    <button class="btn btn-primary" name="proses"> Proses </button>
</form>

<?php
    if(isset($_POST["proses"]))
    {
        $resi = $_POST["resi"];
        $status = $_POST["status"];
        $koneksi->query("update pembelian set resi_pengiriman = '$resi', status_pembelian= '$status' 
        where id_pembelian = '$id_pem' ");
        echo "<script>alert('Data Sudah Di Update'); location= 'index.php?halaman=pembelian';</script>";
    }
?>