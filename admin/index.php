﻿<?php
session_start();
include '../koneksi.php';

if(!isset($_SESSION['admin']))
{
    ?>
    <script>
    alert('Anda Harus Login Terlebih dahulu');
    location='login.php';
    </script>
    <?php
}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/png" sizes="32x32" href="../foto_produk/fav.png">
    <title>Admin</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
     <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Binary admin</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;">&nbsp; <a href="login.html" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="assets/img/find_user.png" class="user-image img-responsive"/>
					</li>
				
					
                    <li>
                        <a href="index.php"><i class="fas fa-home fa-2x"></i> Home</a>
                    </li>
                    <li>
                        <a href="index.php?halaman=produk"><i class="fas fa-box-open fa-2x"></i> Produk</a>
                    </li>
                    <li>
                        <a href="index.php?halaman=pembelian"><i class="fas fa-shopping-cart fa-2x"></i> Pembelian</a>
                    </li>
                    <li>
                        <a href="index.php?halaman=pelanggan"><i class="fas fa-users fa-2x"></i> Pelangganan</a>
                    </li>
                    <li>
                        <a href="index.php?halaman=logout"><i class="fas fa-sign-out-alt fa-2x"></i> Logout</a>
                    </li>
                     
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner"> 
                <?php
                if(isset($_GET['halaman'])){
                    if($_GET['halaman']== "produk"){
                        include 'produk.php';
                    }
                    else if($_GET['halaman']== "pembelian"){
                        include 'pembelian.php';
                    }
                    else if($_GET['halaman']== "pelanggan"){
                        include 'pelanggan.php';
                    }
                    else if($_GET['halaman']== "detail"){
                        include 'detail.php';
                    }
                    else if($_GET['halaman']== "tambahproduk"){
                        include 'tambahproduk.php';
                    }
                    else if($_GET['halaman']== "hapusproduk"){
                        include 'hapusproduk.php';
                    }
                    else if($_GET['halaman']== "ubahproduk"){
                        include 'ubahproduk.php';   
                    }
                    else if($_GET['halaman']== "logout"){
                        include 'logout.php';   
                    }
                    else if($_GET['halaman']== "pembayaran"){
                        include 'pembayaran.php';   
                    }

                }
                else{
                    include 'home.php';
                }
                ?>
            </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
     <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
