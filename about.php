<?php
session_start();
include 'bootstrap.php';
include 'koneksi.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" sizes="32x32" href="foto_produk/fav.png">
    <title>About</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    
    <style>
        .ogo{
            margin-top: 6px;
        }
        .aga{
            margin-top: 4px;
        }
    </style>
</head>
<body>
<?php
include 'navbar4.php';
?><br><br>
<div class="container">
<center><h1>Selamat Datang di HERMON RENTAL MOBIL</h1></center><br>
<div class="row">
    <div class="col-8">        
    <div style="text-align:justify;">
    Selamat datang di website kami Car Rent adalah jasa yang melayani persewaan atau rental mobil di kota jakarta dan kota sekitarnya, kami melayani secara professional di dukung team manageman dan sopir yang handal. kami pengalaman melayani perjalanan kemanapun tujuan anda. apapun kepentingan anda, baik bisnis, perbankan, perusahaan, akademi , dinas pemerintah, pariwisata, seminar, acara keluarga.
    Kami menyediakan banyak pilihan dari mobil yang ekonomis sampai mobil eksekutif, sesuai dengan keperluan perjalanan dan kebutuhan anda, jenis mobil ; avanza , xenia , innova , apv, dll.
    <br><br>
    <h4>NILAI PLUS DARI CAR RENT :</h4> 
   <p>1. Sedia menjemput di bandara, stasion , hotel , alamat yang di kehendaki.</p>
   <p>2. Sedia menjemput di luar kota Jakarta.</p> 
    <p> 3. Mobil terawat dengan baik, baru, bersih, full ac.</p>
    <p> 4. garasi tidak jauh dari pusat kota, bandara Soekarno Hatta, Stasiun.</p>
    <p> 5. Pengemudi pengalaman rute jalan baik luar kota,dalam kota, tempat tempat wisata, tempat.</p>    
    </div>
    </div>    
    <div class="col-4">
    <img src="foto_produk/about.png" width=80% >
    </div>
</div>
</div>
<br><br>
<?php
include 'footer.php';
?>
</body>
</html>