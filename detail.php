<?php include 'koneksi.php';
    include 'bootstrap.php';
session_start();
    $id_produk = $_GET["id"];
    $ambil= $koneksi->query("select * from produk where id_produk = '$id_produk'");
    $detail = $ambil->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"> 
    <link rel="icon" type="image/png" sizes="32x32" href="foto_produk/fav.png">   
    <title>Detail</title>           
    <style>
        .ogo{
            margin-top: 6px;
        }
        .aga{
            margin-top: 4px;
        }
    </style> 
    <link rel="stylesheet" href="css/detail.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
</head>
<body>
    <?php include 'navbar4.php'; ?>

    <section class="konten">
        <div class="container">
            <div class="row">                
                <div class="col-md-6">
                    <img src="foto_produk/<?php echo $detail['foto_produk']; ?>" id="image-container" class="img-responsive ukk">
                </div>
                <div class="col-md-6">
                <br>
                    <h2><?php echo $detail['nama_produk']; ?></h2>
                    <div style="color: #ff5722; font-weight:550; font-size:3rem;">Rp. <?php echo number_format($detail['harga_produk']); ?></div>                        
                    
                    <p><?php echo $detail['deskripsi_produk']; ?></p>
                        <div style="margin-top: 2.5rem; padding: 0 2rem">  
                        <form method="post" class="form-horizontal">
                        <div class="form-group">
                            <div class="row">
                            <label class="col-md-3">Kuantitas</label>
                            <div class="col-md-3">
                                <input type="number" min="1" class="form-control" name="jumlah" max="<?php echo $detail['stok_produk']; ?>">                                  
                            </div>
                            <div class="col-md-6">
                                        <span>Tersisa <?php echo $detail['stok_produk']; ?> Buah</span>
                            </div>
                            </div>
                        </div>
                        <div>
                           <button class="btn btn-solid-primary" name="beli">Rent</button>
                        </div>
                        </form>                                                        
                            
                            <?php
                        if(isset($_POST["beli"]))
                        {
                            $jumlah = $_POST["jumlah"];
                            $_SESSION["keranjang"][$id_produk] = $jumlah;

                            echo "<script> alert('Produk Telah Masuk Ke Keranjang'); location='keranjang.php' </script>";
                        }                    
                    ?>                                                                           
                        </div>
                           
                </div>
            </div>
                               
            <br>
            <div class="row">
                <div class="col-md-2">
                    <img onclick="change_img(this)" src="foto_produk/<?php echo $detail['foto_produk']; ?>" class="img-responsive ukr">
                </div>
                <div class="col-md-2">
                    <img onclick="change_img(this)" src="foto_produk/<?php echo $detail['foto_produk2']; ?>" class="img-responsive ukr">
                </div>
                <div class="col-md-2">
                    <img onclick="change_img(this)" src="foto_produk/<?php echo $detail['foto_produk3']; ?>" class="img-responsive ukr">
                </div>
            </div>
        </div>        
        
<!-- cara membuat tabs -->
    <div class="container tabb">
    <hr>
        <ul class="nav justify-content-center navv">
        <li class="nav-item">
            <a class="nav-link active nama" href="#desc" role="tab" data-toggle="tab">Deskripsi Produk</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#spek" role="tab" data-toggle="tab">Fitur</a>
        </li>        
        </ul>
        <div class="tab-content">
            <br>
            <div role="tabpanel" class="tab-pane active" id="desc" style="text-align:justify;">Toyota Fortuner, Sport Utiliity Vehicle (SUV) yang pertama kali diperkenalkan Toyota pada 2005 dan menjadi rival Mitsubishi Pajero Sport. Mobil ini dibuat berdasar platform IMV (Innovative International Multi-purpose Vehicle), platform yang juga digunakan Toyota untuk memproduksi Hilux serta Kijang Innova.

Dalam pemasarannya terbagi menjadi enam varian yaitu SRZ (bensin), G, A/T, G M/T, G A/T 4x4, VRZ A/T 4x2, dan tertinggi VRZ 4x4 A/T.  Toyota Fortuner di Indonesia dijual dalam dua pilihan mesin, yaitu mesin bensin dan diesel. Toyota Fortuner bensin memakai mesin berkapasitas 2,7 liter sedangkan Fortuner diesel memakai mesin berkapasitas 2,4 liter. 

Bisa dikatakan Fortuner jadi tulang punggung penjualan Toyota di segmen SUV. Mobil ini dilengkapi berbagai fitur dan teknologi. Fortuner juga ternyata memiliki beberapa keunikan yang tak dimiliki pesaingnya.</div>
            <div role="tabpanel" class="tab-pane" id="spek" style="text-align:justify;">Fortuner ditawarkan dengan banyak fitur di setiap varian, sehingga membuatnya kompeten di segmen SUV. MEntenagai Fortuner 2.4 VRZ 4x4 A/T adalah mesin 2393 cc yang menghasilkan 148 hp dengan torsi maksimum 401 Nm. Varian tertinggi Fortuner terbilang efisien dengan konsumsi BBM 11.6 kmpl untuk di dalam kota, dan 15.4 kmpl di jalan tol</div>
            
        </div>
    </div>
    <br><br><br><br>haha
    </section>       
<script>
    var container = document.getElementById("image-container");
    function change_img(image){
        container.src = image.src;

    }
</script>
</body>
</html>
