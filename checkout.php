<?php
    session_start();
    include 'bootstrap.php';
    include 'koneksi.php';


if(empty($_SESSION["keranjang"]) or !isset($_SESSION["keranjang"]))
{
    echo "<script>alert('Mohon Belanja Terlebih dahulu'); location= 'index.php';</script>";
}
else if(!isset( $_SESSION["pelanggan"])){
    echo "<script>alert('Silahkan Login dulu'); location= 'daftar.php';</script>";
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />    
    <title>Checkout</title>    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">    
    <link rel="stylesheet" href="css/checkout.css" />    

</head>
<body>
<?php include 'navbar4.php'; ?>

<section class="konten">
    <div class="container">
    
    <br><br>
    <h1>Checkout</h1><br>
    <div class="row">
        <div class="col-md-8">
        
        <h4>Alamat Pengiriman</h4>    
            <div class="card" style="">
                <div class="card-body">
                    <h6 class="card-title"><strong><?php echo $_SESSION["pelanggan"]['nama_pelanggan'] ?></strong></h6>
                    <p class="card-text"><?php echo $_SESSION["pelanggan"]['telepon_pelanggan'] ?></p>
                    
                    <form method="post">
                    <div class="form-group">
                        <label>Alamat Lengkap Penjemputan</label>
                        <textarea name="alamat_penjemputan" class="form-control" placeholder="Masukkan Alamat Beserta Kode Posnya"></textarea>
                    </div> 
                    
                </div>
            </div>
                    
                    <br>
                    <h4>Detail Pembelian</h4> 
                    <?php 
                        $no = 1;
                        $totalbelanja = 0;
                        foreach ($_SESSION["keranjang"] as $id_produk => $jumlah) :
                            // menampilkan produk 
                        $ambil = $koneksi->query("SELECT * FROM produk where id_produk = '$id_produk' ");
                        $pecah = $ambil->fetch_assoc();
                        $subharga = $pecah["harga_produk"]* $jumlah;                    
                    ?>
    
    
                        <div class="card" style="margin-bottom: 10px;">
                            <div class="card-body">
                            <img src="foto_produk/<?php echo $pecah["foto_produk"]; ?>" class="img-responsive gambar" >                        
                                <h5 class="card-title"><?php echo $pecah["nama_produk"]; ?></h5>                                
                                <p class="card-text">Rp. <?php echo number_format($pecah["harga_produk"]); ?></p>
                                <p style="color: gray;">Jumlah: <?php echo $jumlah; ?></p>
                                <hr>
                                <p>Sub Total: <span style="float:right;">Rp.<?php echo number_format($subharga); ?></span></p>
                            </div>
                        </div>

                    <?php
                        $no++;
                        $totalbelanja += $subharga;
                        endforeach 
                    ?>
        </div>

            <div class="col-md-4">
            <h4>Ringkasan Belanja</h4>
                <div class="card" style="width: 18rem;">
                <div class="card-body">                                
                    <p class="card-text">Total Harga: <span style="float:right;">Rp.<?php echo number_format($totalbelanja); ?></span> </p>
                    <p class="kecil">Dengan Ini anda menyetujui <span style="color: green;">syarat dan ketentuan kita</span></p>
                </div>
                <div class="form-group" style="margin-left: 15px; width: 200px;">                 
                    <label>Tanggal Pinjam</label>
                    <input type="date" name="tanggal_pinjam" class="form-control">
                </div>  
                
                <div class="form-group" style="margin-left: 15px; width: 200px;">                 
                    <label>To</label>
                    <input type="date" name="tanggal_kembali" class="form-control" >
                </div>                                                
                <center><button class="btn btn-primary" name="checkout" style="width:200px;">Checkout</button></center>
                <br>
                </form>
            </div>
    </div>
    </div>
    </div>
</section>



<?php
            if(isset($_POST["checkout"]))
            {
                $id_pel = $_SESSION["pelanggan"]["id_pelanggan"];
                $alamat = $_POST['alamat_penjemputan'];
                $tgl_pinjam        = $_POST['tanggal_pinjam'];
                $tgl_kembali        = $_POST['tanggal_kembali'];
                $selisihangka = strtotime($tgl_kembali) -  strtotime($tgl_pinjam);
                $hari = $selisihangka/(60*60*24);
                $total_pembelian = $totalbelanja * $hari;

                //menyimpan data di tabel pembelian
                $koneksi->query("insert into pembelian (id_pelanggan, tanggal_pinjam, tanggal_kembali, total_harga, alamat_penjemputan)
                values ('$id_pel', '$tgl_pinjam', '$tgl_kembali', '$total_pembelian', '$alamat') ");

                // echo $hari;
                // mendapatkan id pembelian barusan
                $id_pembelian_barusan = $koneksi->insert_id;
                foreach($_SESSION["keranjang"] as $id_produk => $jumlah ){
                    $koneksi->query("insert into pembelian_produk(id_pembelian, id_produk, jumlah) 
                    values ('$id_pembelian_barusan', '$id_produk', '$jumlah')");

                    //skrip update stok
                    $koneksi->query("update produk set stok_produk = stok_produk - $jumlah where id_produk= '$id_produk' ");
                }
                //Mengkosongkan keranjang
                unset($_SESSION["keranjang"]);

                //tampilan di alihkan ke nota
                echo "<script> alert('Pembelian sukses');
                location='nota.php?id=$id_pembelian_barusan';
                </script>";
            }
        ?>

    
</body>
</html>














