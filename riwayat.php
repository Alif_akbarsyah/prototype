<?php
    session_start();
    include 'bootstrap.php';
    include 'koneksi.php';
    if(!isset($_SESSION["pelanggan"]) or empty($_SESSION["pelanggan"]))
    {
        echo "<script>alert('Silahkan Login Terlebih dahulu'); location= 'login.php';</script>";
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">    
    <link rel="icon" type="image/png" sizes="32x32" href="foto_produk/fav.png">
    <title>Riwayat</title>
    <style>
        .ogo{
            margin-top: 6px;
        }
        .aga{
            margin-top: 4px;
        }
    </style>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
</head>
<body>

    <?php include 'navbar4.php'; ?>
    <section class="riwayat">
<br><br>    
    <div class="container">
        <h1>Riwayat Belanja <?php echo $_SESSION["pelanggan"]["nama_pelanggan"]; ?> </h1>
        <hr><br>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tanggal Pinjam</th>
                    <th>Tanggal Kembali</th>
                    <th>Status</th>
                    <th>Total</th>
                    <th>Opsi</th>
                </tr>
            </thead>
            <tbody>
            <?php 
                    $no = 1;            
                        // mendapatkan id_pelanggan dari login session
                    $id_pelanggan = $_SESSION["pelanggan"]["id_pelanggan"];
                    $ambil = $koneksi->query("SELECT * FROM pembelian where id_pelanggan = '$id_pelanggan' ");
                    while($pecah = $ambil->fetch_assoc()){
                         
                ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $pecah["tanggal_pinjam"]; ?></td>
                    <td><?php echo $pecah["tanggal_kembali"]; ?></td>
                    <td>
                        <?php echo $pecah["status_pembelian"]; ?>
                        <br>
                        <?php if(!empty($pecah['resi_pengiriman'])): ?>
                         <strong> Resi:  <?php echo $pecah["resi_pengiriman"]; ?></strong>
                        <?php endif ?>   
                    
                    </td>
                    <td>Rp. <?php echo number_format($pecah["total_harga"]); ?></td>                    
                    
                    <td>
                        <a href="nota.php?id=<?php echo $pecah["id_pembelian"] ?>" class="btn btn-info"><i class="fas fa-pencil-alt"></i></a>
                        <?php $angka= date('Y-m-d'); ?>
                        <?php if($pecah['status_pembelian'] == 'pending'): ?>
                        <a href="pembayaran.php?id=<?php echo $pecah["id_pembelian"] ?>" class="btn btn-success"><i class="fas fa-dollar-sign"></i></a>                        
                        <?php endif ?>                        
                        
                        <a href="pengembalian.php?id=<?php echo $pecah["id_pembelian"] ?>" class="btn btn-success"><i class="fas fa-exchange-alt"></i></a>
                        
                    </td>
                    
                </tr>

                <?php
                    $no++;                 
                }
                 ?>
               
            </tbody>

            
</body>
</html>