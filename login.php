<?php
    session_start();
    include 'koneksi.php';
    include 'bootstrap.php';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />    
    <title>Login</title>
    <link rel="stylesheet"  href="css/login.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous"> 
    
</head>
<body>

<section class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-6 col-md-3">
            <div class="bg">
            <i class="fas fa-user-circle fa-5x"></i>
            </div>
            
        <form class="form-container" method="post">
        <h4 class="text-center font-weight-bold">Login Form</h4>
        <div class="form-group">  
            <input type="text" name="username" class="form-control" placeholder="Enter Username">            
            
        </div>
        <div class="form-group">        
            <input type="password" name="pass" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>
      <br>
        <button type="submit" name="simpan" class="btn btn-primary btn-block">Submit</button>
        </form>
        
        </div>
    </div>

</section>



    <!-- <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Login Pelanggan</h3>
                    </div>
                    <div class="panel-body">
                        <form method="post">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="pass">
                            </div>
                            <button class="btn btn-primary" name="simpan">Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    
    <?php
        if(isset($_POST["simpan"]))
        {
            $username= $_POST["username"];
            $pass = $_POST["pass"];
            $ambil = $koneksi->query("SELECT * FROM pelanggan where username = '$username' and password_pelanggan = '$pass' ");
            $cocok = $ambil->num_rows;
            if($cocok == 1)
            {
                $akun = $ambil->fetch_assoc();   
                $_SESSION["pelanggan"]= $akun;
                if(!empty($_SESSION["keranjang"]) or isset($_SESSION["keranjang"]))
                {
                    echo "<script> location= 'checkout.php';</script>";
                }
                else{                    
                ?>
                 <script> location= 'index.php';</script>
                <?php
                }
            }
            else
            {
                ?>
                <script>
                    alert('Gagal Login, Harap Periksa Kembali');
                    location='login.php';
                </script>
                <?php
            }
        }
    ?>
    <!-- <input type="text" onchange="act()" id="ag">
    <input type="text" readonly id="doi">
    <script>
        function act(){
            var tgl_2 = document.getElementById("ag").value;
                
            document.getElementById('doi').value = tgl_2;
        }
    </script> -->

    
</body>
</html>