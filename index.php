<?php
session_start();

include 'koneksi.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />    
    <link rel="icon" type="image/png" sizes="32x32" href="foto_produk/fav.png">
    <title>Home</title>    
    <style>
        .ogo{
            margin-top: 6px;
        }
    </style>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet" href="css/index.css" />        
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
    
    
    
</head>
<body>
<!-- As a link -->
<nav class="navbar navbar-dark bg-dark justify-content-center">
  <h6 style="font-size: 13px; color: #b5aec4; height: 30px; line-height:30px; text-transform: uppercase;">Free Shipping All T-Shirt You Want</h6>
</nav>

<?php include 'navbar4.php' ?>

<!-- konten -->

    <!-- <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
        <img class="d-block w-100" src="foto_produk/wall2.jpg" alt="First slide">
        </div>
        <div class="carousel-item">
        <img class="d-block w-100" src=".../800x400?auto=yes&bg=666&fg=444&text=Second slide" alt="Second slide">
        </div>
        <div class="carousel-item">
        <img class="d-block w-100" src=".../800x400?auto=yes&bg=555&fg=333&text=Third slide" alt="Third slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    </div> -->


    <div id="home">
        <div class="container">
        <div class="landing-text">
        <div class="row">
        <div class="col-7">
        <h2>Get Our T-Shirt </h2>
            <h2>Now!</h2>
            <br>
            <a href="shop.php" class="btn btn-danger btn-lg">Rent Now</a>
        </div>
            <div class="col-5">
            
            </div>
        </div>
        </div>
        </div>
    </div>
    <div class="padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <img src="foto_produk/innova.jpg" alt="">
            </div>
            <div class="col-sm-6 text-center">
                <h2>What is Car Rent?</h2>
                <p class="lead">
                  Car Rent is a website that provides high quality cars and is ready to be loaned to you and guarantees full customer satisfaction. We also have reliable and trusted drivers. 
                  We provide many choices from economical cars to executive cars. Wait moreover, let's borrow a car with us and enjoy your trip with our car
                </p>    
            </div>
        </div>
    </div>
    </div>

<div class="padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-1">
                <h4>Built With Small Company</h4>
                <p style="text-align: justify;"> Car Rent is a small company that starts with small capital and buys several familiar cars to rent at affordable prices </p>                
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-1">
                    <img src="foto_produk/logoku.jpg" class="img-responsive">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-1">
                <h4>With Trusted Drivers</h4>
                <p style="text-align: justify;"> Car Rent has drivers who are professional in their field and honest in their duties 
                and already familiar with travel routes in the area. Therefore, you do not need to worry about the drivers given by us. </p>                
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-1">
            <img src="foto_produk/driver.jpg" class="img-responsive">
            </div>
        </div>
    </div>
</div>

<div class="fixedd">
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</div>


<div style="background-color:#eee; padding-top:25px;">
<center><h1>Produk Baru</h1></center> 
    <div class="container">
        <div class="padding">
        <div class="sliderr">    
        <?php                            
                $jok =  $koneksi->query("select * 
                from produk order by tanggal_ditambahkan desc limit 7") or die(mysqli_error());
                while($per = $jok->fetch_assoc()){                                        
        ?>
    <div class="col-4">
        
        <div class="card" style="width: 16rem;">
            <img class="card-img-top" height="260" src="foto_produk/<?php echo $per['foto_produk'] ?>" >
            <div class="card-body">
                <h5 class="card-title"><?php echo $per['nama_produk'] ?></h5>
                <p class="card-text">Rp. <?php echo number_format($per['harga_produk']) ?></p>
                <p class="card-text"><?php echo $per['brand'] ?></p>
                <a href="beli.php?id=<?php echo $per['id_produk']; ?>" class="btn btn-primary">Rent</a>    
                <a href="detail.php?id=<?php echo $per['id_produk']; ?>" class="btn btn-warning">Detail</a>    
            </div>
        </div>
        
    </div>        
        <?php
            }
        ?>  
        </div>
        </div>
    </div>
</div>



<br>


<?php
include 'footer.php';
?>



<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js" ></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <script>
$(function() {   
 $('.sliderr').slick({
     slidesToShow: 4,
     slidesToScroll: 1,
     
     autoplaySpeed: 2000,
  });
});
</script>  

</body>
</html>

