<!-- <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
    <link rel="stylesheet" href="css/detail.css"/>
    <style>
      .tabb{
    max-width: 800px;
}
ul {
    list-style: none;
    padding: 0;
}
    li {
      display: inline-flex;
    }  
a{
    text-decoration: none;
    color: rgb(0, 255, 76);
    padding: 10px;
    transition: all .3s ease-in-out;
    border-bottom: 1px solid transparent;    
}
a:hover {
    color: rgb(252, 2, 2);
  }
.navv .active a{ 
    color: rgb(121, 81, 81); 
    border-bottom: 1px solid slateblue;    
}
.navv .active a:hover {
    border-color: transparent;
    background: slateblue;
    color: white;
  }
  .tab-pane { 
    display: none;
  }
  
  .tab-pane.active { 
    display: block; 
  }
    </style>
</head>
<body>
<div class="container">
  
  <ul id="nav-tab" class="nav">
    <li class="active"><a href="#home">Home</a></li>
    <li><a href="#profile">Profile</a></li>
    <li><a href="#messages">Messages</a></li>
    <li><a href="#settings">Settings</a></li>
  </ul>

  
  <div class="tab-content">
    <div class="tab-pane active" id="home">
      <h4>Home Panel Content</h4> 
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem iure quos cum, saepe reprehenderit minima quasi architecto numquam nesciunt dicta. Qui excepturi recusandae vitae maiores, inventore sequi? Rerum, odio omnis.</p> </div>
    <div class="tab-pane" id="profile">
      <h4>Profile Panel</h4>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem iure quos cum, saepe reprehenderit minima quasi architecto numquam nesciunt dicta. Qui excepturi recusandae vitae maiores, inventore sequi? Rerum, odio omnis. Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias distinctio, tempora incidunt aliquid adipisci, minus rerum optio libero quae provident sed at dignissimos, quia nostrum! Fuga dolorum quia hic magni.</p></div>
    <div class="tab-pane" id="messages">
      <h4>Messages Panel</h4>
      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fugiat quos, at qui aspernatur minus animi hic sunt necessitatibus incidunt molestiae reprehenderit ratione neque odit ipsa. Nemo laborum consequatur adipisci beatae!</p>
    </div>
    <div class="tab-pane" id="settings">
      <h4>Settings Panel</h4>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima possimus sed odit iste vitae, magnam amet illum laudantium ea! Fugiat consectetur consequuntur qui eos obcaecati sequi ipsam repellat vero voluptate.</p>
    </div>
  </div>
</div>
</body>
</html> -->
<?php
include 'koneksi.php';
include 'bootstrap.php';
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Page Title</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" media="screen" href="main.css" />  
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>

  <style>
    body{
      background-color:blue; 
    }

  </style>  
</head>
<body>
<div style="width:60vw;">
<div class="slide">
        <?php                            
                $jok =  $koneksi->query("select * 
                from produk order by tanggal_ditambahkan desc limit 7") or die(mysqli_error());
                while($per = $jok->fetch_assoc()){                                        
        ?>
    <div class="col-3">
        
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" height="160" src="foto_produk/<?php echo $per['foto_produk'] ?>" >
            <div class="card-body">
                <h5 class="card-title"><?php echo $per['nama_produk'] ?></h5>
                <p class="card-text">Rp. <?php echo number_format($per['harga_produk']) ?></p>
                <p class="card-text"><?php echo $per['brand'] ?></p>
                <a href="beli.php?id=<?php echo $per['id_produk']; ?>" class="btn btn-primary">Rent</a>    
                <a href="detail.php?id=<?php echo $per['id_produk']; ?>" class="btn btn-warning">Detail</a>    
            </div>
        </div>
        
    </div>        
    <?php
        }
   ?>  
</div>

  </div>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>    
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <script>
$(function() {   
 $('.slide').slick({
     slidesToShow: 3,
     slidesToScroll: 1,
     autoplay: true,
     autoplaySpeed: 2000,
  });
});
</script>  
</body>
</html>