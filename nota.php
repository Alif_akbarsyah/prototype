<?php
    session_start();
    include 'koneksi.php';
    include 'bootstrap.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">    
    <title>Nota Pembelian</title>
    <link rel="icon" type="image/png" sizes="32x32" href="foto_produk/fav.png">        
    <style>
        .ogo{
            margin-top: 6px;
        }
        .aga{
            margin-top: 4px;
        }
    </style>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
</head>
<body>
    <?php
        include 'navbar4.php';
    ?>
    <br>

    <section class="konten">
        <div class="container">
        <h2>Detail Pembelian</h2>

    <?php
        $no =1;
        $ambil= $koneksi->query("select * from pembelian join pelanggan on pembelian.id_pelanggan 
        = pelanggan.id_pelanggan where pembelian.id_pembelian= '$_GET[id]'");
            $pecah = $ambil->fetch_assoc();
    ?>
     <?php
        $idpelangganygbeli = $pecah["id_pelanggan"];

        $idpelangganyglogin = $_SESSION["pelanggan"]["id_pelanggan"];

        if($idpelangganygbeli!=$idpelangganyglogin)
        {
            ?> <script>alert('Jangan Nakal Ya');
                location='riwayat.php';
            </script>
            <?php
        }
    ?>

    <div class="row">
        <div class="col-md-4">
            <h3>Pelanggan</h3>            
            <strong><?php echo $pecah['nama_pelanggan']; ?></strong> <br>
            <p>
            <?php echo $pecah['telepon_pelanggan']; ?><br>
            <?php echo $pecah['email_pelanggan']; ?>
            </p>
        </div>
        <div class="col-md-4">
            <h3>Pembelian</h3>
            <strong>No. Pembelian: <?php echo $pecah['id_pembelian']; ?></strong><br>
            Rp. <?php echo number_format($pecah['total_harga']); ?>
            
        </div>
        <div class="col-md-4">
        <h3>Peminjaman</h3>
        Dari: <strong><?php echo $pecah['tanggal_pinjam']; ?><br></strong>
        Sampai: <strong><?php echo $pecah['tanggal_kembali']; ?> <br></strong> 
        Alamat Penjemputan : <?php echo $pecah['alamat_penjemputan']; ?> 
        </div>   
    </div>        
              
       <table class="table table-bordered" >

            <tr>
            <th>No.</th>
            <th>Nama Mobil</th>
            <th>Harga </th>
            <th>Jumlah</th>
            <th>SubTotal</th>                  
            </tr>
            <tr>
                <?php
                $no =1;
                    $ambil= $koneksi->query("select * from pembelian_produk join produk on pembelian_produk.id_produk
                = produk.id_produk where pembelian_produk.id_pembelian= '$_GET[id]'");
                while($detail = $ambil->fetch_assoc()){
                ?>
               
            

                <td><?php echo $no ?></td>
                <td><?php echo $detail['nama_produk']; ?></td>
                <td><?php echo $detail['harga_produk']; ?> </td>
                <td><?php echo $detail['jumlah']; ?> </td>
                <td><?php echo $detail['harga_produk'] * $detail['jumlah']; ?> </td>
            </tr>
            <?php
            $no++;
                    }
                ?>
        </table>
            <div class="row">
                <div class="col-md-7">
                    <div class="alert alert-info">
                        <p>
                            Silahkan Melakukan Pembayaran Rp. <?php echo number_format($pecah['total_harga']); ?> ke <br>
                            <strong>BANK BCA 138-001088-2231 AN. Muhammad Nashrudin</strong>
                        </p>
                    </div>
                </div>
            
            </div>
        </div>
    </section>
    
</body>
</html>