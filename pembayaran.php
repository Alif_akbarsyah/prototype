<?php
    session_start();
    include 'koneksi.php';
    if(!isset($_SESSION["pelanggan"]) or empty($_SESSION["pelanggan"]))
    {
        echo "<script>alert('Silahkan Login Terlebih dahulu'); location= 'login.php';</script>";
        exit();
    }

$idpem= $_GET['id'];
$ambil = $koneksi->query("select * from pembelian where id_pembelian = '$idpem'");
$detpem = $ambil->fetch_assoc();

$idpelangganygbeli = $detpem["id_pelanggan"];

$idpelangganyglogin = $_SESSION["pelanggan"]["id_pelanggan"];

    if($idpelangganygbeli != $idpelangganyglogin)
        {
            ?> <script>alert('Jangan Nakal Ya');
                location='riwayat.php';
            </script>
            <?php
        }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" sizes="32x32" href="foto_produk/fav.png">
    <title>Pembayaran</title>    
    <link rel="stylesheet" href="admin/assets/css/bootstrap.css" />    
</head>
<body>
    <?php include 'navbar.php'; ?>

    <div class="container">
        <h2>Konfirmasi</h2>
        <p>Kirim Bukti Pembayaran Disini</p>
            <div class="alert alert-info"> Total Tagihan Anda <strong> Rp. <?php echo number_format($detpem["total_harga"]) ?> </strong> </div>

        <form method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label>Nama Penyetor</label>
                <input type="text" class="form-control" name="nama">
            </div>
            <div class="form-group">
                <label>Bank</label>
                <input type="text" class="form-control" name="bank">
            </div>
            <div class="form-group">
                <label>Jumlah</label>
                <input type="text" class="form-control" name="jumlah" min="1">
            </div>
            <div class="form-group">
                <label>Foto Bukti</label>
                <input type="file" class="form-control" name="bukti" >
                <p class="text-danger">Foto Bukti Harus JPG dan maksimal 2MB</p>                
            </div>
            <button class="btn btn-primary" name="kirim">Kirim</button>
        </form>        
    </div>
    <?php
        if(isset($_POST["kirim"]))
        {
            $namabukti = $_FILES["bukti"]["name"];
            $lokasibukti = $_FILES["bukti"]["tmp_name"];
            $namafiks = date("YmdHis").$namabukti;
            move_uploaded_file($lokasibukti, "bukti_pembayaran/$namafiks");

            $nama = $_POST["nama"];
            $bank = $_POST["bank"];
            $jumlah = $_POST["jumlah"];
            $tanggal= date("Y-m-d");

            //simpan data pembayaran
            $masuk = $koneksi->query("insert into pembayaran (id_pembelian, nama_pembayaran, bank, jumlah_pembayaran, tanggal, bukti) 
            values ('$idpem', '$nama','$bank', '$jumlah', '$tanggal', '$namafiks' ) ") or die(mysqli_error($koneksi))  ;

            //update status dari pending menjadi berhasil 
            if($masuk){
            $koneksi->query("update pembelian set status_pembelian = 'sudah kirim pembayaran' where id_pembelian ='$idpem' ");
            echo "<script>alert('Terima Kasih Sudah Kirim Pembayarannya'); location= 'riwayat.php';</script>";
            }
            else{
                echo 'error';
            }
        }

    ?>
</body>
</html>