<?php
session_start();
include 'bootstrap.php';
include 'koneksi.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />    
    <link rel="icon" type="image/png" sizes="32x32" href="foto_produk/fav.png">
    <title>Rent</title>    
    <style>
        .ogo{
            margin-top: 6px;
        }
        .aga{
            margin-top: 4px;
        }
    </style>
    <link rel="stylesheet" href="css/shop.css" />    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <script type="text/javascript" src="js/jquery-3.1.0.min.js"></script>
    
</head>
<body>

<?php include 'navbar4.php' ?>

<!-- konten -->
<br><br>    
<section>

    <div class="container">

        <div class="row">
            <div class="col-3">
                <h3>Category</h3>
            </div>
            <div class="col-9">
                <center> <h3>Featured Items</h3> </center>
            </div>
        </div>
        <div class="row">            
        
        <div class="col-3">
            <div class="card" style="width: 13rem;" >
                <div class="card-body"> 
                    <h6 class="card-title"><a href="#" style="color: black; text-decoration: none;" class="tarik">
                        Brand <i style="margin-right: 0;" class="fas fa-plus"></i>
                    </a></h6>                                
                    <div class="penerima">
                    <form method="post">
                        <?php
                            $ambil = $koneksi->query("select distinct(brand) from produk order by id_produk desc");
                            while($pecah = $ambil->fetch_assoc()){
                        ?>                        
                        <li>
                                <input type="submit" class="btn butt" name="benda" value="<?php echo $pecah["brand"] ?>">                                                              
                                </li>
                        <?php
                            }
                            ?>                        
                                </form>
                        
                    </div>
                </div>
            </div>
        </div>    
        
        <div class="col-9">
        <h1>Produk Baru</h1>
    <div class="row">
                <?php
                
                if(isset($_POST["benda"])){
                    
                    $merk = $_POST["benda"];
                    // var_dump($merk);
                    $ambil = $koneksi->query("select * from produk where brand = '$merk' and stok_produk >0 ") or die(mysqli_error());
                    
                    
                    while($perproduk = $ambil->fetch_assoc()){
                        ?>
                        <div class="col-4">
                            <div class="card-deck">
                                <div class="card coba" style="width: 18rem;">
                                    <img class="card-img-top" height="160" src="foto_produk/<?php echo $perproduk['foto_produk'] ?>" >
                                    <div class="card-body">
                                        <h5 class="card-title"><?php echo $perproduk['nama_produk'] ?></h5>
                                        <p class="card-text">Rp. <?php echo number_format($perproduk['harga_produk']) ?></p>
                                        <p class="card-text"><?php echo $perproduk['brand'] ?></p>
                                        <a href="beli.php?id=<?php echo $perproduk['id_produk']; ?>" class="btn btn-primary">Rent</a>    
                                        <a href="detail.php?id=<?php echo $perproduk['id_produk']; ?>" class="btn btn-warning">Detail</a>    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                                
                            
                        } 
                                          
                }
                else{
                $ambil= $koneksi->query("select * from produk where stok_produk >0");
                    while($perproduk = $ambil->fetch_assoc()){
                    
            ?>
                <div class="col-4">
                    <div class="card-deck">
                        <div class="card coba" style="width: 18rem; margin-bottom: 15px; margin-left:0px;">
                            <img class="card-img-top" height="260" src="foto_produk/<?php echo $perproduk['foto_produk'] ?>" >
                            <div class="card-body">
                            <a href="detail.php?id=<?php echo $perproduk['id_produk']; ?>" style="text-decoration: none; color: black;">
                                <h5 class="card-title"><?php echo $perproduk['nama_produk'] ?></h5>
                            </a>    
                                <p class="card-text">Rp. <?php echo number_format($perproduk['harga_produk']) ?></p>
                                <p class="card-text"><?php echo $perproduk['brand'] ?></p>
                                <a href="beli.php?id=<?php echo $perproduk['id_produk']; ?>" class="btn btn-primary btn-lg btn-block">Buy</a>    
                            
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                        
                    }}
            ?>
    </div>
        </div>           
        </div>
    </div>
    <div>    

    </div>
</section>

<!-- Footer -->
<?php include 'footer.php' ?>
<script src="js/shop.js"></script>
</body>

</html>