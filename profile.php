<?php
session_start();
include 'bootstrap.php';
include 'koneksi.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" sizes="32x32" href="foto_produk/fav.png">    
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <style>
        .bgt{
            border-bottom: 1px solid #EEE;
            list-style: none;
            line-height: 26px;
            text-align: left;
            padding: 5px 0;
        }
        .tap{
            border-left: 2px solid #eee;
            border-right: 2px solid #eee;
            padding: 0 20px;
        }
        .ogo{
            margin-top: 6px;
        }
        .aga{
            margin-top: 4px;
        }
    </style>
</head>
<body>
<?php
include 'navbar4.php';
?>
<br>
    <div class="container">
    <div class="row">
    <div class="col-5">
    
        <img src="foto_produk/walll.jpg" width="100%">
    </div>
    <div class="col-7 tap">    
    <?php    
        $amb =   $_SESSION["pelanggan"]['id_pelanggan'];
        $ambil = $koneksi->query("SELECT * FROM pelanggan where id_pelanggan = '$amb' ");
        $pecah = $ambil->fetch_assoc();  
    ?>
            <h2><?php echo $pecah["nama_lengkap"]; ?></h2>
            <ul style="margin: 20px 0; padding: 0px;">
                <li class="bgt"><i class="fas fa-envelope"></i>&nbsp<?php echo $pecah["email_pelanggan"]; ?></li> 
                <li class="bgt"><i class="fas fa-phone"></i>&nbsp<?php echo $pecah["telepon_pelanggan"]; ?></li>
                <li class="bgt"><i class="fas fa-calendar-alt"></i>&nbsp<?php echo $pecah["tanggal_lahir"]; ?></li>
                
            </ul>
            
    </div>
    </div>
    </div>
</body>
</html>